FROM openjdk:7

COPY target/SpringCloudConfigurationServer-*.jar /SpringCS.jar

CMD ["java", "-jar", "/SpringCS.jar"]